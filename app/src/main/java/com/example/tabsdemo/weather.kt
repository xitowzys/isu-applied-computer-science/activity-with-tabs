package com.example.tabsdemo

data class Weather(
    var city: String,
    var temp: Float,
    var type: String,
    var typeImg: Int
)